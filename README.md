Example 7: How Transactions Go Wrong
========

An example concerning Dates, Mutability, and `static` Fields and Methods

### Objectives of this Example
+ Understand the keyword `final` and also why it is not sufficient
+ Understand mutability of objects (and when there is undesired mutation)
+ Understand `static` class fields and `static` methods
+ See one of the most simple uses of `JUnit`

### Enabling `JUnit` in Eclipse

After cloning this repository and creating an Eclipse project, to run the `JUnit` test that is part of the source code, one will need to add `JUnit` to the Java Build Path. In Eclipse, this can be done through the menu sequence `Project > Properties  > Java Build Path > Add Library …` and then selecting `JUnit 4`.

### The `Transaction` Class

The `Transaction` class in this example is a class that should allow us to record some financial transaction. It is quite elementary in this example to illustrate some key ideas. A sketch of this class follows but the detailed source code is in `Transacation.java`.

```java
public class Transaction {

	// a class variable to generate new transaction identifiers when new
	// transaction objects are created. This variable keeps track of the most
	// recent transaction created.
	static private int recentTransactionId;

	// object fields for transaction amount, transaction date and transaction
	// identifier. All fields are final because they are never changed after the
	// object is created.
	private final int amount;
	private final Calendar date;
	private final int transactionId;

	// constructor to create a new transaction given amount and date
	/**
	 * 
	 * @param amount
	 *            The transaction amount
	 * @param date
	 *            The date of the transaction
	 */
	public Transaction(int amount, Calendar date) {
		…
	}

	/**
	 * A method to get the transaction amount
	 * 
	 * @return transaction amount
	 */
	public int getAmount() {
		…
	}

	/**
	 * A method to get the transaction date
	 * 
	 * @return transaction date
	 */
	public Calendar getDate() {
		…
	}

	/**
	 * Create a new transaction with the same amount as a given transaction but
	 * set the date of the new transaction to be one month later.
	 * 
	 * This is a static method or class method.
	 * 
	 * @param t
	 *            the transaction to use as template
	 * @return a new transaction for the same amount as the input transaction
	 *         but with date one month later
	 */
	public static Transaction getNextMonthTransaction(Transaction t) {
		…
	}
}
```

### Running the `JUnit` Test

To run the test that is in `TransactionTest.java`, open that file in Eclipse and then select `Run > Run As… > JUnit Test`.

The test does some sanity checking:
```java
public void testGetNextMonthTransaction() {
		Calendar d = new GregorianCalendar( );
		int amount = 100;
		
		Transaction t1 = new Transaction( amount, d );
		Transaction t2 = Transaction.getNextMonthTransaction( t1 );
		
		// this should be the date for transaction t1
		d = new GregorianCalendar( );
		
		// was the original transaction unchanged
		// let us compare the months of the two dates
		assertEquals( t1.getDate( ).get(Calendar.MONTH), d.get(Calendar.MONTH) );
		
		d.add(Calendar.MONTH, 1);				
		// was the new transaction created correctly
		assertEquals( t2.getAmount( ), amount );
		assertEquals( t2.getDate( ).get(Calendar.MONTH), d.get(Calendar.MONTH) );
	}
```

You should see a `JUnit` sidebar (normally, depending on your Eclipse settings) where you can see the result of the test.

### A Failed Test

The provided test will fail. 
+ Why does it fail? 
+ Is this really a failure or should this be appropriate behaviour for our code?

### Things to Ponder

+ Why were `date`, `amount` and `transactionId` in `Transaction` declared to be `final`?
+ Why did the use of the keyword `final` not help?
+ Why did we declare `recentTransactionId` in `Transaction` to be `static`? 
+ Is the declaration of `getNextMonthTransaction( )` as `static` a reasonable choice? What alternative approach could we have taken for this method?

### References
+ Start reading about the `static` and `final` keywords with the Oracle [Java tutorial](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html).
+ [More on Class Variables](http://docs.oracle.com/javase/tutorial/java/javaOO/classvars.html)
+ [Tutorial](http://www.vogella.com/tutorials/JUnit/article.html) on using `JUnit` for unit testing
+ Some of you may also want to look at the [`Calendar`](http://docs.oracle.com/javase/7/docs/api/java/util/Calendar.html) class.
